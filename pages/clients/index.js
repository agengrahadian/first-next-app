import Link from 'next/link';

function ClientsPage(){
    const clients = [
        { id : "eris", name : "Eris"},
        { id : "supardi", name : "Supardi"},
        { id : "rudy", name : "Rudy"},
    ]

    return (
        <div>
            <h1>The Client Page</h1>
            <ul>
                {/* You can use this */}
                {/* {clients.map( (client) => (
                    <li key={client.id}>
                        <Link href={`/clients/${client.id}`}>{client.name}</Link>
                    </li>
                ))} */}
                {/* or alternatively you can use this */}
                {clients.map((client) => (
                    <li key={client.id}>
                        <Link href={{
                            pathname: "/clients/[id]",
                            query:{id: client.id}, 
                        }}>{client.name}</Link>
                    </li> 
                ))}
            </ul>
        </div>
    )
}

export default ClientsPage;